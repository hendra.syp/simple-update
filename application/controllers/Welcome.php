<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
  function __construct(){ 
    parent::__construct();
      $this->load->helper('url'); 
      $this->load->helper('file');    
      header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
      header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
      header("Cache-Control: post-check=0, pre-check=0", false);
      header("Pragma: no-cache");

      header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    }
  
    function getCurrent(){
      $file = "versioning.txt";
      $handle = fopen($file, "r");
      $readLines  = file($file);
      $lineCounts = count($readLines);
      $currentVersion = 0;
      $latestVersion = 0;
    
    if ($handle) {
      $currentVersion = $readLines[$lineCounts-1];
      // while (($line = fgets($handle)) !== false) {
      //echo count(file("versioning.txt"));
      //  echo $readLines[$latestVersion]."<br/>";
      //}
        fclose($handle);
    } else {
        // error opening the file.
    }
    return $currentVersion;
      
    }
    public function suhe(){
      echo "suhendra";
    }
    public function recent(){
      echo "suhendra";
    }
    public function update(){
      shell_exec("git pull origin master");
    }
    function getLatest(){
      $file = 'http://www.fokusnews.com/wp-versioning/versibaru.txt?r='.rand(1,1000);
      $handle = fopen($file, "r");
      $readLines  = file($file);
      $lineCounts = count($readLines);
      $currentVersion = 0;
      $latestVersion = 0;
       if ($handle) {
      $currentVersion = $readLines[$lineCounts-1];
      // while (($line = fgets($handle)) !== false) {
      //echo count(file("versioning.txt"));
      //  echo $readLines[$latestVersion]."<br/>";
      //}
        fclose($handle);
      } else {
          // error opening the file.
      }
      return $currentVersion;
    }
	public function index()
	{
    $currentVersion = $this->getCurrent();
    $latestVersion = $this->getLatest();
    
    echo $currentVersion. ' = '. $latestVersion;

    
    
		$this->load->view('welcome_message',['currentVersion'=>$currentVersion,'latestVersion'=>$latestVersion]);
	}
}
